import json
from jsonschema import validate, SchemaError, ValidationError
import glob
import logging
import os.path


def find_schema(event):
    filename = 'schema/' + event + '.schema'
    if os.path.isfile(filename):
        with open(filename, 'r') as fp:
            return json.load(fp)
    return False


if __name__ == '__main__':
    eventNames = glob.glob('event/*.json')
    logging.basicConfig(filename='result.log', filemode='w', format='%(asctime)s - %(message)s')

    for name in eventNames:
        with open(name, 'r') as fp:
            data = json.load(fp)
        schema = False
        if data and 'event' in data:
            schema = find_schema(data['event'])
        if schema:
            try:
                validate(instance=data, schema=schema)
            except SchemaError as err:
                logging.error('Error in schema ' + data['event'] + '.schema ' + err.message)
            except ValidationError as err:
                logging.error('Error in file ' + name + '. ' + err.message)
        else:
            logging.error('We have not schema for file ' + name)
